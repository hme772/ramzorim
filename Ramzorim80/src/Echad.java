import java.awt.Color;

import javax.swing.JPanel;

/*
 * Created on Mimuna 5767  upDate on Tevet 5770 
 */

/**
 * @author �����
 */
enum OutState {
	  ON,OFF
	}

class Echad extends Thread
{
	Ramzor ramzor;
	JPanel panel;
	OutState outState;
	public Echad( Ramzor ramzor,JPanel panel)
	{
		this.ramzor=ramzor;
		this.panel=panel;
		start();
	}

	public void run()
	{
		outState=OutState.OFF;
		try 
		{
			while (true)
			{
				switch(outState) {
				
				case ON:
					setLight(1,Color.YELLOW);
					sleep(1000);
					outState=OutState.OFF;
					break;
				case OFF:
					setLight(1,Color.GRAY);
					sleep(1000);
					outState=OutState.ON;
					break;
				default:
					break;
				}
			}
		} catch (InterruptedException e) {}

	}

	public void setLight(int place, Color color)
	{
		ramzor.colorLight[place-1]=color;
		panel.repaint();
	}
}
