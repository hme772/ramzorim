import java.awt.Color;

import javax.swing.JPanel;

/*
 * Created on Mimuna 5767  upDate on Tevet 5770 
 */

/**
 * @author �����
 */

enum OutState2 {
	  ON_CHOL,ON_SHABAT
	}
enum InState {
	  ON_GREEN,ON_RED 
	}
class ShneyLuchot extends Thread
{
	Ramzor ramzor;
	JPanel panel;
	OutState2 outerState;
	InState innerState;
	Event64 evToGreen, evToRed, evToShabat, evToChol, evAck;
	public ShneyLuchot( Ramzor ramzor,JPanel panel ,Event64 evToGreen, Event64 evToRed, Event64 evToShabat,
			Event64 evToChol, Event64 evAck)
	{
		super();
		this.ramzor=ramzor;
		this.panel=panel;
		this.evToGreen = evToGreen;
		this.evToRed = evToRed;
		this.evToShabat = evToShabat;
		this.evToChol = evToChol;
		this.evAck = evAck;
		start();
	}

	public void run()
	{
		outerState=OutState2.ON_CHOL;
		innerState=InState.ON_RED;//the start point
		//first light is red and the second on if off
		setLight(1,Color.RED);
		setLight(2,Color.GRAY);
		try 
		{
			while (true)
			{
				switch(outerState)
				{
				case ON_CHOL:
					while(outerState==OutState2.ON_CHOL)
					{
						switch(innerState)
						{
						case ON_RED:
							
							while(true)
							{

								
								if(evToGreen.arrivedEvent())
								{
									//if we are in red and we recieved evToGreen we need to turn on the green light
									evToGreen.waitEvent(); //check if that is for me etc.
									setLight(1,Color.GRAY);
									setLight(2,Color.GREEN);
									innerState=InState.ON_GREEN;
									break;
								}
								else
									if(evToShabat.arrivedEvent())
									{
										evToShabat.waitEvent();
										setLight(1,Color.GRAY);
										setLight(2,Color.GRAY);
										outerState=OutState2.ON_SHABAT;
										break;
									}
									else
									{
										yield();
										
									}
							}
							break;
						case ON_GREEN:
							while(true)
							{
								

								if(evToRed.arrivedEvent())
								{
									evToRed.waitEvent();
									setLight(1,Color.RED);
									setLight(2,Color.GRAY);
									evAck.sendEvent();
									innerState=InState.ON_RED;
									break;
									
								}
								if(evToShabat.arrivedEvent())
								{
									evToShabat.waitEvent();
									setLight(1,Color.GRAY);
									setLight(2,Color.GRAY);
									outerState=OutState2.ON_SHABAT;
									break;
								}
								else
								{
									yield();
								}
								break;
							}
						default:
							break;
						}
					}
					break;
				case ON_SHABAT:
					//in case of shabat we wait to go back to CHOL and return to the start
					evToChol.waitEvent();
					setLight(1,Color.RED);
					setLight(2,Color.GRAY);
					innerState=InState.ON_RED;
					outerState=OutState2.ON_CHOL;
					break;
				default:
					break;
				}
			}
		} catch (Exception e) {}

	}
	public void setLight(int place, Color color)
	{
		ramzor.colorLight[place-1]=color;
		panel.repaint();
	}
}
