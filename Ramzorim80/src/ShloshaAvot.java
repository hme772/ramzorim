import java.awt.Color;

import javax.swing.JPanel;

/*
 * Created on Mimuna 5767  upDate on Tevet 5770 
 */

/**
 * @author �����
 */
enum OutStateVehicle {
	ON_CHOL,ON_SHABAT
}
enum InStateChol {
	ON_GREEN,ON_BLINKING_GREEN,ON_RED,ON_ORANGE,ON_RED_ORANGE,GREEN_OFF
}

enum InStateShabat {
	ORANGE_ON,ORANGE_OFF
}

enum InStateGreen {
	GREEN_ON,GREEN_OFF
}
public class ShloshaAvot extends Thread
{
	Ramzor ramzor;
	JPanel panel;
	private boolean stop=true;
	OutStateVehicle outerState;
	InStateChol inStateChol;
	InStateShabat inStateShabat;
	InStateGreen inStateGreen;
	int count=0;
	Event64 evToGreen, evToRed, evToShabat, evToChol, evAck;
	
	public ShloshaAvot( Ramzor ramzor,JPanel panel,int count,Event64 evToGreen,
			Event64 evToRed, Event64 evToShabat, Event64 evToChol, Event64 evAck)
	{
		this.ramzor=ramzor;
		this.panel=panel;
		this.count=count;
		this.evToGreen = evToGreen;
		this.evToRed = evToRed;
		this.evToShabat = evToShabat;
		this.evToChol = evToChol;
		this.evAck = evAck;
		new CarsMaker(panel,this,count);
		start();
	}

	public void run()
	{
		outerState=OutStateVehicle.ON_CHOL;
		try 
		{
			while (true)
			{
				switch(outerState)
				{
				case ON_CHOL:
					inStateChol=InStateChol.ON_RED;
					setLight(1,Color.RED);
					setLight(2,Color.LIGHT_GRAY);
					setLight(3,Color.LIGHT_GRAY);
					
					while(outerState==OutStateVehicle.ON_CHOL)
					{

						switch(inStateChol)
						{
						case ON_RED:
							while(true)
							{
								if(evToGreen.arrivedEvent())
								{
									evToGreen.waitEvent();
									setLight(1,Color.RED);
									setLight(2,Color.ORANGE);
									setLight(3,Color.LIGHT_GRAY);
									inStateChol=InStateChol.ON_RED_ORANGE;
									break;
								}
								else if(evToShabat.arrivedEvent())
								{
									evToShabat.waitEvent();
									stop=true;
									setLight(1,Color.LIGHT_GRAY);
									setLight(2,Color.LIGHT_GRAY);
									setLight(3,Color.LIGHT_GRAY);
									outerState=OutStateVehicle.ON_SHABAT;
									break;
								}
								else
									yield();
							}
							break;
						case ON_RED_ORANGE:
							
							if(evToShabat.arrivedEvent())
							{
								evToShabat.waitEvent();
								stop=true;
								setLight(1,Color.LIGHT_GRAY);
								setLight(2,Color.LIGHT_GRAY);
								setLight(3,Color.LIGHT_GRAY);
								outerState=OutStateVehicle.ON_SHABAT;
								break;
							}//if did not get shabat but got 1000 tm
							sleep(1400);
							setLight(1,Color.LIGHT_GRAY);
							setLight(2,Color.LIGHT_GRAY);
							setLight(3,Color.GREEN);
							stop=false;//to stop the car    
							inStateChol=InStateChol.ON_GREEN;
							break;
						case ON_GREEN:
							while(true)
							{								
								if(evToRed.arrivedEvent())
								{
									evToRed.waitEvent();
									count=0; //so it will blink 3 times 
									inStateChol=InStateChol.ON_BLINKING_GREEN;
									break;
								}
								else if(evToShabat.arrivedEvent())
								{
									evToShabat.waitEvent();
									stop=true;
									setLight(1,Color.LIGHT_GRAY);
									setLight(2,Color.LIGHT_GRAY);
									setLight(3,Color.LIGHT_GRAY);
									outerState=OutStateVehicle.ON_SHABAT;
									break;
								}
								else
									yield();
							}
							break;
						case ON_BLINKING_GREEN:
							setLight(1,Color.LIGHT_GRAY);
							setLight(2,Color.LIGHT_GRAY);
							setLight(3,Color.LIGHT_GRAY);
							inStateGreen=InStateGreen.GREEN_OFF;
							while(outerState==OutStateVehicle.ON_CHOL && inStateChol==InStateChol.ON_BLINKING_GREEN)
							{
								if(evToShabat.arrivedEvent())
								{
									evToShabat.waitEvent();
									stop=true;
									setLight(1,Color.LIGHT_GRAY);
									setLight(2,Color.LIGHT_GRAY);
									setLight(3,Color.LIGHT_GRAY);
									outerState=OutStateVehicle.ON_SHABAT;
									break;
								}
								else if(count!=3)
								{
									switch(inStateGreen)
									{
									case GREEN_ON:
										if(evToShabat.arrivedEvent())
										{
											evToShabat.waitEvent();
											stop=true;
											setLight(1,Color.LIGHT_GRAY);
											setLight(2,Color.LIGHT_GRAY);
											setLight(3,Color.LIGHT_GRAY);
											outerState=OutStateVehicle.ON_SHABAT;
											break;
										}
										sleep(1000);
										setLight(1,Color.LIGHT_GRAY);
										setLight(2,Color.LIGHT_GRAY);
										setLight(3,Color.LIGHT_GRAY);
										count++;
										inStateGreen=inStateGreen.GREEN_OFF;
										break;
									case GREEN_OFF:
										if(evToShabat.arrivedEvent())
										{
											evToShabat.waitEvent();
											stop=true;
											setLight(1,Color.LIGHT_GRAY);
											setLight(2,Color.LIGHT_GRAY);
											setLight(3,Color.LIGHT_GRAY);
											outerState=OutStateVehicle.ON_SHABAT;
											break;
										}
										sleep(1000);
										setLight(1,Color.LIGHT_GRAY);
										setLight(2,Color.LIGHT_GRAY);
										setLight(3,Color.GREEN);
										stop=false;
										inStateGreen=inStateGreen.GREEN_ON;
										break;
									default:
										break;
									}
								}
								else //count=3
								{
									stop=true;
									setLight(1,Color.LIGHT_GRAY);
									setLight(2,Color.ORANGE);
									setLight(3,Color.LIGHT_GRAY);
									inStateChol=InStateChol.ON_ORANGE;
									break;
								}
							}
							break;
						case ON_ORANGE:
							while(true)
							{

								if(evToShabat.arrivedEvent())
								{
									evToShabat.waitEvent();
									stop=true;
									setLight(1,Color.LIGHT_GRAY);
									setLight(2,Color.LIGHT_GRAY);
									setLight(3,Color.LIGHT_GRAY);
									outerState=OutStateVehicle.ON_SHABAT;
									break;
								}
								sleep(1900);
								setLight(1,Color.RED);
								setLight(2,Color.LIGHT_GRAY);
								setLight(3,Color.LIGHT_GRAY);
								evAck.sendEvent();
								inStateChol=InStateChol.ON_RED;
								break;
							}
							break;
						default:
							break;	
						}
					}	
					break;
				case ON_SHABAT:
					inStateShabat=inStateShabat.ORANGE_OFF;
					while (outerState==OutStateVehicle.ON_SHABAT)
					{
						switch(inStateShabat)
						{
						case ORANGE_OFF:

							if(evToChol.arrivedEvent())
							{
								evToChol.waitEvent();
								outerState=OutStateVehicle.ON_CHOL;
								setLight(1,Color.RED);
								setLight(2,Color.LIGHT_GRAY);
								setLight(3,Color.LIGHT_GRAY);
								break;
							}
							sleep(1000);
							setLight(1,Color.LIGHT_GRAY);
							setLight(2,Color.ORANGE);
							setLight(3,Color.LIGHT_GRAY);
							inStateShabat=InStateShabat.ORANGE_ON;
							break;
						case ORANGE_ON:
							
							if(evToChol.arrivedEvent())
							{
								evToChol.waitEvent();
								outerState=OutStateVehicle.ON_CHOL;
								setLight(1,Color.RED);
								setLight(2,Color.LIGHT_GRAY);
								setLight(3,Color.LIGHT_GRAY);
								break;
							}
							sleep(1000);
							setLight(1,Color.LIGHT_GRAY);
							setLight(2,Color.LIGHT_GRAY);
							setLight(3,Color.LIGHT_GRAY);
							inStateShabat=InStateShabat.ORANGE_OFF;
							break;

						default:
							break;
						}


					}
					break;
				default:
					break;
				}
			}
		} catch (Exception e) {}

	}
	public void setLight(int place, Color color)
	{
		ramzor.colorLight[place-1]=color;
		panel.repaint();
	}

	public boolean isStop()
	{
		return stop;
	}
}
