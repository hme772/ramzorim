import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JRadioButton;

/*
 * Created on Tevet 5770 
 */

/**
 * @author �����
 */


public class MyActionListener implements ActionListener
{
	Event64 evToCholButt,evToShabatButt,evWalkerButt;

	public MyActionListener(Event64 evToCholButt, Event64 evToShabatButt, Event64 evWalkerButt) {
		super();
		this.evToCholButt = evToCholButt;
		this.evToShabatButt = evToShabatButt;
		this.evWalkerButt = evWalkerButt;
	}

	public void actionPerformed(ActionEvent e) 
	{
		JRadioButton butt=(JRadioButton)e.getSource();
		//System.out.println(butt.getName());
		//		butt.setEnabled(false);
		//		butt.setSelected(false);
		if(butt.getName().equals("16"))
		{
			if(butt.isSelected())
				evToShabatButt.sendEvent();
			else
				evToCholButt.sendEvent();
		}
		else
			evWalkerButt.sendEvent(butt.getName());
	}

}
