import javax.swing.JFrame;

/*
 * Created on Mimuna 5767  upDate on Tevet 5770 
 */

/**
 * @author �����
 */

public class TrafficLightFrame extends JFrame 
{
	private final int WIDTH = 800, HEIGHT = 750;
	TrafficLightPanel myPanel;

	public TrafficLightFrame(String h, Ramzor[] ramzorim) 
	{
		super(h);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		myPanel = new TrafficLightPanel(ramzorim);
		add(myPanel);
		setSize(WIDTH, HEIGHT);
		setLocationRelativeTo(null);
		setResizable(false);
		setVisible(true);
	}
}

