import com.oracle.jrockit.jfr.EventInfo;

import sun.awt.EventListenerAggregate;

/*
 * 1    =0,  9 , 10 , 12, 13, 6 , 7 
 * 2    =1, 2, 4, 5, 6, 7, 12, 13
 * 3    = 3, 4, 5, 8, 11, 9, 10, 14, 15
 */



enum OutStateController //states of hol and shabat
{
	ON_CHOL,ON_SHABAT
}

enum InStateController //states of the groups
{
	START,GROUP_1_GREEN, GROUP_1_RED,TEMP_1_TO_2, GROUP_2_GREEN, GROUP_2_RED,TEMP_2_TO_3, GROUP_3_GREEN, GROUP_3_RED,TEMP_3_TO_1
}
public class Controller extends Thread
{
	Event64[] evToShabatArr,evToCholArr, evToRedArr,evAckOnRedArr, evToGreenArr;
	Event64 evToShabatButt,evToCholButt,evPressButton;
	InStateController inState;
	OutStateController outerState;
	private int x;
	MyTimer72 timer;
	Event64   evTimer;



	//CTOR
	public Controller(Event64[] evToShabatArr, Event64[] evToCholArr, Event64[] evToRedArr, Event64[] evOnRedArr,
			Event64[] evToGreenArr, Event64 evToShabatButt, Event64 evToCholButt, Event64 evWalker) {
		super();
		this.evToShabatArr = evToShabatArr;
		this.evToCholArr = evToCholArr;
		this.evToRedArr = evToRedArr;
		this.evAckOnRedArr = evOnRedArr;
		this.evToGreenArr = evToGreenArr;
		this.evToShabatButt = evToShabatButt;
		this.evToCholButt = evToCholButt;
		this.evPressButton = evWalker;
		start();
	}




	public void run()
	{
		try {
			outerState=OutStateController.ON_CHOL;
			while (true) {
				switch (outerState) {
				case ON_CHOL:
					evTimer=new Event64();
					timer=  new MyTimer72(5000,evTimer);
					inState=InStateController.START;					//sendAllStart();
					while(outerState==OutStateController.ON_CHOL)
					{
						switch (inState) {
						case START:

									send1ToGreen();
									inState=InStateController.GROUP_1_GREEN;
									
									break;

						case GROUP_1_GREEN:
							while(true)
							{
								
								if(evToShabatButt.arrivedEvent())
								{
									evToShabatButt.waitEvent();
									sendAllShabat();
									outerState=OutStateController.ON_SHABAT;
									break;
								}
								else if(evTimer.arrivedEvent())
								{
									evTimer.waitEvent();
									send1ToRed();
									inState=InStateController.GROUP_1_RED;
									break;
								}
								else
									yield();
							}
							break;
						case GROUP_1_RED:
							while (true)
							{
								//send1ToRed();
								if(evToShabatButt.arrivedEvent())
								{
									evToShabatButt.waitEvent();
									sendAllShabat();
									outerState=OutStateController.ON_SHABAT;
									break;
								}
								else if (ack1InRed())
								{
									resetAck1();
									evTimer=new Event64();
									timer=  new MyTimer72(2000,evTimer);
									inState=InStateController.TEMP_1_TO_2;
									break;

								}

								else
									yield();
							}
							break;
						case TEMP_1_TO_2:
							while(true) 
							{
								if(evToShabatButt.arrivedEvent())
								{
									evToShabatButt.waitEvent();
									sendAllShabat();
									outerState=OutStateController.ON_SHABAT;
									break;
								}
								else if(evTimer.arrivedEvent())
								{
									send2ToGreen();
									evTimer=new Event64();
									timer=  new MyTimer72(5000,evTimer);
									
									inState=InStateController.GROUP_2_GREEN;
									break;
								}
								else if(evPressButton.arrivedEvent())
								{
									x=Integer.parseInt((evPressButton.waitEvent()).toString());
									if(check2()) 
									{
										send2ToGreen();
										inState=InStateController.GROUP_2_GREEN;
										
									}
									else if(check3()) 
									{
										send3ToGreen();
										inState=InStateController.GROUP_3_GREEN;
									}
									else if(check1()) 
									{
										send1ToGreen();
										inState=InStateController.GROUP_1_GREEN;
									}
									evTimer=new Event64();
									timer=  new MyTimer72(5000,evTimer);
									break;
								}
								else
									yield();

							}
							break;
						case GROUP_2_GREEN:
							while(true)
							{
								
								if(evToShabatButt.arrivedEvent())
								{
									evToShabatButt.waitEvent();
									sendAllShabat();
									outerState=OutStateController.ON_SHABAT;
									break;
								}
								else if(evTimer.arrivedEvent())
								{
									send2ToRed();									
									inState=InStateController.GROUP_2_RED;	
									break;
								}
								
								else
									yield();
							}
							break;

						case GROUP_2_RED:
							while (true)
							{
								
								if(evToShabatButt.arrivedEvent())
								{
									evToShabatButt.waitEvent();
									sendAllShabat();
									outerState=OutStateController.ON_SHABAT;
									break;
								}
								else if (ack2InRed())
								{
									resetAck2();
									evTimer=new Event64();
									timer=  new MyTimer72(2000,evTimer);
									inState=InStateController.TEMP_2_TO_3;
									break;

								}

								else
									yield();
							}
							break;
						case TEMP_2_TO_3:
							while(true)
							{
								if(evToShabatButt.arrivedEvent())
								{
									evToShabatButt.waitEvent();
									sendAllShabat();
									outerState=OutStateController.ON_SHABAT;
									break;
								}
								else if(evTimer.arrivedEvent())
								{
									send3ToGreen();
									evTimer=new Event64();
									timer=  new MyTimer72(5000,evTimer);
									inState=InStateController.GROUP_3_GREEN;
									break;
								}
								
								else if(evPressButton.arrivedEvent())
								{
									x=Integer.parseInt((evPressButton.waitEvent()).toString());
									if(check3()) 
									{
										send3ToGreen();
										inState=InStateController.GROUP_3_GREEN;
									}
									else if(check1()) 
									{
										send1ToGreen();
										inState=InStateController.GROUP_1_GREEN;
									}
									else if(check2()) 
									{
										send2ToGreen();
										inState=InStateController.GROUP_2_GREEN;
									}
									evTimer=new Event64();
									timer=  new MyTimer72(5000,evTimer);
									break;
								}
								
								else
									yield();
							}
							break;
						
						case GROUP_3_GREEN:
							while(true)
							{
								
								if(evToShabatButt.arrivedEvent())
								{
									evToShabatButt.waitEvent();
									sendAllShabat();
									outerState=OutStateController.ON_SHABAT;
									break;
								}
								else if(evTimer.arrivedEvent())
								{
									send3ToRed();
									inState=InStateController.GROUP_3_RED;
									break;
								}
								else
									yield();
							}
							break;
						case GROUP_3_RED:
							while (true)
							{
								
								if(evToShabatButt.arrivedEvent())
								{
									evToShabatButt.waitEvent();
									sendAllShabat();
									outerState=OutStateController.ON_SHABAT;
									break;
								}
								else if (ack3InRed())//to keep the red light on for 8 seconds
								{
									resetAck3();
									evTimer=new Event64();
									timer=  new MyTimer72(2000,evTimer);
									inState=InStateController.TEMP_3_TO_1;
									break;

								}

								else
									yield();
							}
							break;
						case TEMP_3_TO_1:
							while(true)
							{
								if(evToShabatButt.arrivedEvent())
								{
									evToShabatButt.waitEvent();
									sendAllShabat();
									outerState=OutStateController.ON_SHABAT;
									break;
								}
								else if(evTimer.arrivedEvent())
								{
									send1ToGreen();
									evTimer=new Event64();
									timer=  new MyTimer72(5000,evTimer);
									inState=InStateController.GROUP_1_GREEN;
									break;
								}
								
								else if(evPressButton.arrivedEvent())
								{
									x=Integer.parseInt((evPressButton.waitEvent()).toString());
									if(check1()) 
									{
										send1ToGreen();
										inState=InStateController.GROUP_1_GREEN;
										
									}
									else if(check2()) 
									{
										send2ToGreen();
										inState=InStateController.GROUP_2_GREEN;
									}
									else if(check3()) 
									{
										send3ToGreen();
										inState=InStateController.GROUP_3_GREEN;
									}
									evTimer=new Event64();
									timer=  new MyTimer72(5000,evTimer);
									break;
								}
								
								
								else
									yield();
							}
							break;
					default:
							break;
						}
					}
					break;
				case ON_SHABAT:
					evToCholButt.waitEvent();
					sendAllEvToChol();
					outerState=OutStateController.ON_CHOL;
					break;


				default:
					break;
				}

			}

		} catch (Exception e) {
			// TODO: handle exception
		}


	}




///help functions
	private void sendAllStart()
	{// starts with all the ramzorim turning on red
		for(int i=0;i<16;i++)
			evToRedArr[i].sendEvent();
	}
	
	private void sendAllShabat() { //turn shabat on
		
		for (int i=0;i<evToShabatArr.length;i++)
			evToShabatArr[i].sendEvent();

	}

	private void sendAllEvToChol() { //turn chol on
		for (int i=0;i<evToCholArr.length;i++)
			evToCholArr[i].sendEvent();

	}

	
	private void send1ToGreen() 
	{
		//0, 9, 10, 12, 13, 6 , 7 goes to green
		evToGreenArr[0].sendEvent();
		evToGreenArr[6].sendEvent();
		evToGreenArr[7].sendEvent();
		evToGreenArr[9].sendEvent();		
		evToGreenArr[10].sendEvent();
		evToGreenArr[12].sendEvent();
		evToGreenArr[13].sendEvent();
	}	
	
	private void send1ToRed() 
	{
		//0, 9, 10, 12, 13, 6 , 7 goes to red
		evToRedArr[0].sendEvent();
		evToRedArr[6].sendEvent();
		evToRedArr[7].sendEvent();
		evToRedArr[9].sendEvent();		
		evToRedArr[10].sendEvent();
		evToRedArr[12].sendEvent();
		evToRedArr[13].sendEvent();
	}	
	
	private void send2ToGreen() 
	{
		//1, 2, 4, 5, 6, 7, 12, 13 goes to green
		evToGreenArr[1].sendEvent();
		evToGreenArr[2].sendEvent();
		evToGreenArr[4].sendEvent();
		evToGreenArr[5].sendEvent();
		evToGreenArr[6].sendEvent();
		evToGreenArr[7].sendEvent();
		evToGreenArr[12].sendEvent();
		evToGreenArr[13].sendEvent();

	}	
	
	private void send2ToRed() 
	{
		//1, 2, 4, 5, 6, 7, 12, 13 goes to red
		evToRedArr[1].sendEvent();
		evToRedArr[2].sendEvent();
		evToRedArr[4].sendEvent();
		evToRedArr[5].sendEvent();
		evToRedArr[6].sendEvent();
		evToRedArr[7].sendEvent();
		evToRedArr[12].sendEvent();
		evToRedArr[13].sendEvent();
	}	
	private void send3ToGreen() 
	{
		//3, 4, 5, 8, 9, 10, 11, 14, 15 goes to green
		evToGreenArr[3].sendEvent();
		evToGreenArr[4].sendEvent();
		evToGreenArr[5].sendEvent();
		evToGreenArr[8].sendEvent();
		evToGreenArr[9].sendEvent();
		evToGreenArr[10].sendEvent();		
		evToGreenArr[11].sendEvent();
		evToGreenArr[14].sendEvent();
		evToGreenArr[15].sendEvent();

	}	
	
	private void send3ToRed() 
	{
		//3, 4, 5, 8, 9, 10, 11, 14, 15 goes to red
		evToRedArr[3].sendEvent();
		evToRedArr[4].sendEvent();
		evToRedArr[5].sendEvent();
		evToRedArr[8].sendEvent();
		evToRedArr[9].sendEvent();
		evToRedArr[10].sendEvent();		
		evToRedArr[11].sendEvent();
		evToRedArr[14].sendEvent();
		evToRedArr[15].sendEvent();
	}	
	
	private boolean ack1InRed() { //if got ack from all the ramzorim in group 1
		return evAckOnRedArr[0].arrivedEvent() && evAckOnRedArr[6].arrivedEvent()&& evAckOnRedArr[9].arrivedEvent()&&
				evAckOnRedArr[7].arrivedEvent() && evAckOnRedArr[12].arrivedEvent()&& evAckOnRedArr[13].arrivedEvent();
	}
	private boolean ack2InRed() { //if got ack from all the ramzorim in group 2
		
		return evAckOnRedArr[1].arrivedEvent()&& evAckOnRedArr[2].arrivedEvent()&& evAckOnRedArr[4].arrivedEvent()&& evAckOnRedArr[5].arrivedEvent()&&
				evAckOnRedArr[6].arrivedEvent() && evAckOnRedArr[7].arrivedEvent()&& evAckOnRedArr[12].arrivedEvent()&& evAckOnRedArr[13].arrivedEvent();
	}

	private boolean ack3InRed() { //if got ack from all the ramzorim in group 3
		return evAckOnRedArr[3].arrivedEvent()&& evAckOnRedArr[4].arrivedEvent()&& evAckOnRedArr[5].arrivedEvent()&&
				evAckOnRedArr[8].arrivedEvent() && evAckOnRedArr[9].arrivedEvent()&& evAckOnRedArr[10].arrivedEvent()&&
				evAckOnRedArr[11].arrivedEvent()&& evAckOnRedArr[14].arrivedEvent()&& evAckOnRedArr[15].arrivedEvent();
	}
	
	private boolean check1() { //if a button is pressed from group 1
		return   x==0 || x==6 || x==7 || x==12|| x==13 || x==9|| x==10;
	}

	private boolean check2() { //if a button is pressed from group 2
		return   x==1 ||  x==2 || x==4 || x==5 || x==6 || x==7|| x==12|| x==13;
	}


	private boolean check3() { //if a button is pressed from group 3
		return   x==3 || x==4 || x==5 || x==8|| x==9|| x==10  || x==11|| x==14|| x==15;
	}


	private void resetAck1() 
	{//0, 12, 13, 6 , 7 , 9, 10 resets their ack
		evAckOnRedArr[0].waitEvent();
		evAckOnRedArr[6].waitEvent();
		evAckOnRedArr[7].waitEvent();
		evAckOnRedArr[9].waitEvent();
		evAckOnRedArr[10].waitEvent();
		evAckOnRedArr[12].waitEvent();
		evAckOnRedArr[13].waitEvent();

	}
	private void resetAck2() 
	{//1, 2, 4, 5, 6, 7, 12, 13 resets their ack
		evAckOnRedArr[1].waitEvent();
		evAckOnRedArr[2].waitEvent();
		evAckOnRedArr[4].waitEvent();
		evAckOnRedArr[5].waitEvent();
		evAckOnRedArr[6].waitEvent();
		evAckOnRedArr[7].waitEvent();
		evAckOnRedArr[12].waitEvent();
		evAckOnRedArr[13].waitEvent();

	}
	

	private void resetAck3() 
	{//3, 4, 5, 8, 9, 10, 11, 14, 15 resets their ack

		evAckOnRedArr[3].waitEvent();
		evAckOnRedArr[4].waitEvent();
		evAckOnRedArr[5].waitEvent();
		evAckOnRedArr[8].waitEvent();
		evAckOnRedArr[11].waitEvent();
		evAckOnRedArr[9].waitEvent();
		evAckOnRedArr[10].waitEvent();
		evAckOnRedArr[14].waitEvent();
		evAckOnRedArr[15].waitEvent();

	}

	
	
	


	
	


	
}
